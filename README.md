# vfs
virtual file system for Python 2.x  
it emulates a shell with some commands listed below.  
vfs doesn't store the login password or hash of a vfs file,  
it only visualizes the given password for the user.  
just for my personal purposes, use KeePassX instead.  

main reason for this to exist is the fact that  
python code can be executed and that  
the `parse` function can be used to extract data  
from a vfs.

# why
You can store your passwords in vfs and search through

        > search [file] [string]
        [string].user=this
        [string].password=that

You can store python files and execute them:

        > python [file]

# commands
        load                 : load <filesystem/load var>
        info                 : info <file/directory>
        search               : search <file> <pattern>
        sleep                : sleep <seconds>
        help                 : show this help
        edit                 : edit <file> with a editor
        cat                  : cat <file>
        mkdir                : mkdir <new directory>
        cd                   : cd <directory>
        clear                : clear screen
        export               : export <inside src-file> <outside dst-file>
        ren                  : rename <file/directory> <new name>
        exit                 : exit
        ls                   : ls
        import               : import <outside src-file> <inside dst-file>
        rm                   : rm <file/directory>
        login                : login
        save                 : save <filesystem/load var>
        loginhash            : loginhash
        python               : execute pycode inside <file>
        rmdir                : rmdir <directory>

icon by [Lopagof](http://lopagof.deviantart.com/)
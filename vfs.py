#!/usr/bin/python
# -*- coding: utf8 -*-
import time
import os
import atexit
from hashlib import sha512 as ahash
from hashlib import md5
from getpass import getpass as input_pass
def deltmp():
    for tmp in filter(lambda x: str(x).endswith('.tmp'), os.listdir('.')):
        if os.path.exists(tmp): os.remove(tmp)
atexit.register(deltmp)
'''
This is a port of Chris Veness' AES implementation: http://www.movable-type.co.uk/scripts/aes.html
Copyright ©2005-2008 Chris Veness. Right of free use is granted for all
commercial or non-commercial use. No warranty of any form is offered.
Ported in 2009 by Markus Birth <markus@birth-online.de>
'''
import base64
import datetime
import math
'''Sbox is pre-computed multiplicative inverse in GF(2^8) used in SubBytes and KeyExpansion [§5.1.1]'''
Sbox = [
    0x63,0x7c,0x77,0x7b,0xf2,0x6b,0x6f,0xc5,0x30,0x01,0x67,0x2b,0xfe,0xd7,0xab,0x76,
    0xca,0x82,0xc9,0x7d,0xfa,0x59,0x47,0xf0,0xad,0xd4,0xa2,0xaf,0x9c,0xa4,0x72,0xc0,
    0xb7,0xfd,0x93,0x26,0x36,0x3f,0xf7,0xcc,0x34,0xa5,0xe5,0xf1,0x71,0xd8,0x31,0x15,
    0x04,0xc7,0x23,0xc3,0x18,0x96,0x05,0x9a,0x07,0x12,0x80,0xe2,0xeb,0x27,0xb2,0x75,
    0x09,0x83,0x2c,0x1a,0x1b,0x6e,0x5a,0xa0,0x52,0x3b,0xd6,0xb3,0x29,0xe3,0x2f,0x84,
    0x53,0xd1,0x00,0xed,0x20,0xfc,0xb1,0x5b,0x6a,0xcb,0xbe,0x39,0x4a,0x4c,0x58,0xcf,
    0xd0,0xef,0xaa,0xfb,0x43,0x4d,0x33,0x85,0x45,0xf9,0x02,0x7f,0x50,0x3c,0x9f,0xa8,
    0x51,0xa3,0x40,0x8f,0x92,0x9d,0x38,0xf5,0xbc,0xb6,0xda,0x21,0x10,0xff,0xf3,0xd2,
    0xcd,0x0c,0x13,0xec,0x5f,0x97,0x44,0x17,0xc4,0xa7,0x7e,0x3d,0x64,0x5d,0x19,0x73,
    0x60,0x81,0x4f,0xdc,0x22,0x2a,0x90,0x88,0x46,0xee,0xb8,0x14,0xde,0x5e,0x0b,0xdb,
    0xe0,0x32,0x3a,0x0a,0x49,0x06,0x24,0x5c,0xc2,0xd3,0xac,0x62,0x91,0x95,0xe4,0x79,
    0xe7,0xc8,0x37,0x6d,0x8d,0xd5,0x4e,0xa9,0x6c,0x56,0xf4,0xea,0x65,0x7a,0xae,0x08,
    0xba,0x78,0x25,0x2e,0x1c,0xa6,0xb4,0xc6,0xe8,0xdd,0x74,0x1f,0x4b,0xbd,0x8b,0x8a,
    0x70,0x3e,0xb5,0x66,0x48,0x03,0xf6,0x0e,0x61,0x35,0x57,0xb9,0x86,0xc1,0x1d,0x9e,
    0xe1,0xf8,0x98,0x11,0x69,0xd9,0x8e,0x94,0x9b,0x1e,0x87,0xe9,0xce,0x55,0x28,0xdf,
    0x8c,0xa1,0x89,0x0d,0xbf,0xe6,0x42,0x68,0x41,0x99,0x2d,0x0f,0xb0,0x54,0xbb,0x16
]
'''Rcon is Round Constant used for the Key Expansion [1st col is 2^(r-1) in GF(2^8)] [§5.2]'''
Rcon = [
    [0x00, 0x00, 0x00, 0x00],
    [0x01, 0x00, 0x00, 0x00],
    [0x02, 0x00, 0x00, 0x00],
    [0x04, 0x00, 0x00, 0x00],
    [0x08, 0x00, 0x00, 0x00],
    [0x10, 0x00, 0x00, 0x00],
    [0x20, 0x00, 0x00, 0x00],
    [0x40, 0x00, 0x00, 0x00],
    [0x80, 0x00, 0x00, 0x00],
    [0x1b, 0x00, 0x00, 0x00],
    [0x36, 0x00, 0x00, 0x00]
]
def Cipher(input, w):
    Nb = 4
    Nr = len(w)/Nb - 1

    state = [ [0] * Nb, [0] * Nb, [0] * Nb, [0] * Nb ]
    for i in range(0, 4*Nb): state[i%4][i//4] = input[i]

    state = AddRoundKey(state, w, 0, Nb)

    for round in range(1, Nr):
        state = SubBytes(state, Nb)
        state = ShiftRows(state, Nb)
        state = MixColumns(state, Nb)
        state = AddRoundKey(state, w, round, Nb)

    state = SubBytes(state, Nb)
    state = ShiftRows(state, Nb)
    state = AddRoundKey(state, w, Nr, Nb)

    output = [0] * 4*Nb
    for i in range(4*Nb): output[i] = state[i%4][i//4]
    return output

def SubBytes(s, Nb):
    for r in range(4):
        for c in range(Nb):
            s[r][c] = Sbox[s[r][c]]
    return s

def ShiftRows(s, Nb):
    t = [0] * 4
    for r in range (1,4):
        for c in range(4): t[c] = s[r][(c+r)%Nb]
        for c in range(4): s[r][c] = t[c]
    return s

def MixColumns(s, Nb):
    for c in range(4):
        a = [0] * 4
        b = [0] * 4
        for i in range(4):
            a[i] = s[i][c]
            b[i] = s[i][c]<<1 ^ 0x011b if s[i][c]&0x80 else s[i][c]<<1
        s[0][c] = b[0] ^ a[1] ^ b[1] ^ a[2] ^ a[3]
        s[1][c] = a[0] ^ b[1] ^ a[2] ^ b[2] ^ a[3]
        s[2][c] = a[0] ^ a[1] ^ b[2] ^ a[3] ^ b[3]
        s[3][c] = a[0] ^ b[0] ^ a[1] ^ a[2] ^ b[3]
    return s

def AddRoundKey(state, w, rnd, Nb):
    for r in range(4):
        for c in range(Nb):
            state[r][c] ^= w[rnd*4+c][r]
    return state

def KeyExpansion(key):
    Nb = 4
    Nk = len(key)/4
    Nr = Nk + 6
    w = [0] * Nb*(Nr+1)
    temp = [0] * 4
    for i in range(Nk):
        r = [key[4*i], key[4*i+1], key[4*i+2], key[4*i+3]]
        w[i] = r

    for i in range(Nk, Nb*(Nr+1)):
        w[i] = [0] * 4
        for t in range(4): temp[t] = w[i-1][t]
        if i%Nk == 0:
            temp = SubWord(RotWord(temp))
            for t in range(4): temp[t] ^= Rcon[i/Nk][t]
        elif Nk>6 and i%Nk == 4:
            temp = SubWord(temp)
        for t in range(4): w[i][t] = w[i-Nk][t] ^ temp[t]
    return w

def SubWord(w):
    for i in range(4): w[i] = Sbox[w[i]]
    return w

def RotWord(w):
    tmp = w[0]
    for i in range(3): w[i] = w[i+1]
    w[3] = tmp
    return w

def AesEncrypt(plaintext, password, nBits):
    blockSize = 16
    if not nBits in (128, 192, 256): return ''
#    plaintext = plaintext.encode('utf-8')
#    password  = password.encode('utf-8')
    nBytes = nBits//8
    pwBytes = [0] * nBytes
    for i in range(nBytes): pwBytes[i] = 0 if i>=len(password) else ord(password[i])
    key = Cipher(pwBytes, KeyExpansion(pwBytes))
    key += key[:nBytes-16]
    counterBlock = [0] * blockSize
    now = datetime.datetime.now()
    nonce = time.mktime( now.timetuple() )*1000 + now.microsecond//1000
    nonceSec = int(nonce // 1000)
    nonceMs  = int(nonce % 1000)
    for i in range(4): counterBlock[i] = urs(nonceSec, i*8) & 0xff
    for i in range(4): counterBlock[i+4] = nonceMs & 0xff
    ctrTxt = ''
    for i in range(8): ctrTxt += chr(counterBlock[i])
    keySchedule = KeyExpansion(key)
    blockCount = int(math.ceil(float(len(plaintext))/float(blockSize)))
    ciphertxt = [0] * blockCount
    for b in range(blockCount):
        for c in range(4): counterBlock[15-c] = urs(b, c*8) & 0xff
        for c in range(4): counterBlock[15-c-4] = urs(b/0x100000000, c*8)
        cipherCntr = Cipher(counterBlock, keySchedule)
        blockLength = blockSize if b<blockCount-1 else (len(plaintext)-1)%blockSize+1
        cipherChar = [0] * blockLength
        for i in range(blockLength):
            cipherChar[i] = cipherCntr[i] ^ ord(plaintext[b*blockSize+i])
            cipherChar[i] = chr( cipherChar[i] )
        ciphertxt[b] = ''.join(cipherChar)
    ciphertext = ctrTxt + ''.join(ciphertxt)
    ciphertext = base64.b64encode(ciphertext)
    return ciphertext

def AesDecrypt(ciphertext, password, nBits):
    blockSize = 16
    if not nBits in (128, 192, 256): return ''
    ciphertext = base64.b64decode(ciphertext)
#    password = password.encode('utf-8')

    nBytes = nBits//8
    pwBytes = [0] * nBytes
    for i in range(nBytes): pwBytes[i] = 0 if i>=len(password) else ord(password[i])
    key = Cipher(pwBytes, KeyExpansion(pwBytes))
    key += key[:nBytes-16]

    counterBlock = [0] * blockSize
    ctrTxt = ciphertext[:8]
    for i in range(8): counterBlock[i] = ord(ctrTxt[i])

    keySchedule = KeyExpansion(key)

    nBlocks = int( math.ceil( float(len(ciphertext)-8) / float(blockSize) ) )
    ct = [0] * nBlocks
    for b in range(nBlocks):
        ct[b] = ciphertext[8+b*blockSize : 8+b*blockSize+blockSize]
    ciphertext = ct

    plaintxt = [0] * len(ciphertext)

    for b in range(nBlocks):
        for c in range(4): counterBlock[15-c] = urs(b, c*8) & 0xff
        for c in range(4): counterBlock[15-c-4] = urs( int( float(b+1)/0x100000000-1 ), c*8) & 0xff

        cipherCntr = Cipher(counterBlock, keySchedule)

        plaintxtByte = [0] * len(ciphertext[b])
        for i in range(len(ciphertext[b])):
            plaintxtByte[i] = cipherCntr[i] ^ ord(ciphertext[b][i])
            plaintxtByte[i] = chr(plaintxtByte[i])
        plaintxt[b] = ''.join(plaintxtByte)

    plaintext = ''.join(plaintxt)
 #   plaintext = plaintext.decode('utf-8')
    return plaintext

def urs(a, b):
    a &= 0xffffffff
    b &= 0x1f
    if a&0x80000000 and b>0:
        a = (a>>1) & 0x7fffffff
        a = a >> (b-1)
    else:
        a = (a >> b)
    return a
import pickle

#data
def initdata():
    global data
    data = {
     'user': {
        'name':'default',
        'uid' :0,
        'gid' :0
     },
     'type': {
        'unknown'  :0,
        'directory':1,
        'file'     :2
     },
     'shell': {
        'pre' :'/> ',
        'out' :None,
        'fs'  :{},
        'curr':[]
     },
     'auth': {
        'name':'default',
        'pass':'default'
     }
    }
initdata()
def getv(ns, key):
    global data; return data[ns][key];
def setv(ns, key, val):
    global data; data[ns][key]=val; return data[ns][key];
def hask(ns, key):
    global data; return data[ns].has_key(key);
def getfs(key):
    global data;
    if data['shell']['fs'].has_key(key): return data['shell']['fs'][key];
    return None
def getcd():
    global data;
    return data['shell']['curr']
def setfs(key,val):
    global data;
    data['shell']['fs'][key] = val; return data['shell']['fs'][key];
def make(s, **p):
    for elm in p.keys(): s=str(s).replace('$('+str(elm)+')',str(p[elm]));
    return s;
def setpre(pre):
    u=getv('auth','name')
    setv('shell','pre',u+'@'+str(pre)+'> '); return getv('shell','pre')
def refpre():
    return setpre('/'+'/'.join(getcd()));
def getpre():
    return getv('shell','pre')


def hashcheck(data):
    return md5(str(data)).hexdigest()
#timestamp
def timestamp():
    return int(time.time())
#typeof
def typeof(typ):
    if hask('type',typ):
        return getv('type',typ)
    return getv('type','unknown')
#getmeta
def getmeta(obj):
    if obj.has_key('meta'):
        return obj['meta']
    return {}

def isDirectory(obj):
    if getmeta(obj).has_key('type'):
        typ=getmeta(obj)['type']
        return typ==typeof('directory');
    return False
def isFile(obj):
    if getmeta(obj).has_key('type'):
        typ=getmeta(obj)['type']
        return typ==typeof('file');
    return False
def getDirectory(obj):
    if isDirectory(obj):
        return obj['data']
    return None
def getFile(obj):
    if isFile(obj):
        return obj['data']
    return None
def isDirectoryPath(path):
    d=getDirectory(getfs(''))
    t=getcd()[:]
    t.append(path)
    for k in t:
        if not isDirectory(d[k]): return False
    return True
def isFilePath(path):
    d=getDirectory(getfs(''))
    t=getcd()[:]
    t.append(path)
    for k in t:
        if not isFile(d[k]): return False
    return True

#directory object
def d(data):
    #uid=getv('user','uid')
    #gid=getv('user','gid')
    meta = {
         'owner': getv('auth','name'), 
    #    'owner': {
    #        'uid': [uid],
    #        'gid': [gid]
    #    },
        'timestamp': timestamp(),
        'type':      typeof('directory')
    }
    return {'meta':meta, 'data':data}

#file object
def f(data):
    meta = {
        'owner': getv('auth','name'),
        'timestamp': timestamp(),
        'type':      typeof('file')
    }
    return {'meta':meta, 'data':data}


#operations
def _print(*s):
    a=[]
    for e in s: a.append(str(e))
    print(str(' '*1)+str(' '.join(a)))
    
def usage(f,p=True):
    if p:
        _print('Usage:',str(f.__doc__))
        return None
    return str(f.__doc__)

#root
#current dir
#setfs('', d({'System':d({}),'Users':d({'test':d({}),'test2':d({})}),'testfile':f('testdata')}))
setfs('', d({}))

#d(d({'test':{}}),d({'test2':{}}))

def curr_dict():
    d=getDirectory(getfs(''))
    for k in getcd():
        if isFile(d[k]):
            d=getFile(d[k])
        elif isDirectory(d[k]):
            d=getDirectory(d[k])
    return d

#directory operations
def ls(args):
    '''ls'''
    _print('contents of directory', '/' + '/'.join(getcd()))
    if len(args)==1 and str(args[0]).upper()=='-A':
        for i in curr_dict(): _print('',i)
        return
    for i in curr_dict():
        if i[:1]!='.': _print('',i)

def cd(args):
    '''cd <directory>'''
    if len(args) != 1: return usage(cd);
    if args[0] == '..':
        if len(getcd()) == 0:
            _print('cannot go above root')
        else:
            getcd().pop()
            refpre()
    elif args[0] not in curr_dict():
        _print('directory',args[0],'not found')
    elif isFilePath(args[0]):
        _print('this is a file, not a Directory')
    #elif isDirectoryPath(args[0]):
    else:
        getcd().append(args[0])
        refpre()
    #else: print 'Unknown <directory>'

def mkdir(args):
    '''mkdir <new directory>'''
    if len(args)!=1: return usage(mkdir);
    curr_dict()[str(args[0])] = d({})
def rmdir(args):
    '''rmdir <directory>'''
    if len(args)!=1: return usage(rmdir);
    d=str(args[0])
    if curr_dict().has_key(d) and isDirectory(curr_dict()[d]):
        del curr_dict()[d]
        _print('delete directory')
    elif curr_dict().has_key(d) and isFile(curr_dict()[d]):
        _print('not a directory')
    else:
        _print('directory does not exist')

#file operations
def getpass(args=[]):
    '''getpass'''
    u=getv('auth','name')
    p=getv('auth','pass')
    return str(ahash(str(u)*20+str(p)*20).hexdigest())


def encrypt(data, key):
    return AesEncrypt(data, key, 256);
def decrypt(data, key):
    return AesDecrypt(data, key, 256);

def fwrite(args=[]):
    '''fwrite file data'''
    if len(args)!=2: return usage(fwrite);
    fil=args[0]; d=args[1];
    if curr_dict().has_key(fil) and isDirectory(curr_dict()[fil]): return False
    #if not isDirectory(curr_dict()[fil]):
    if fil and d:
        p=getpass()
        d=encrypt(d,p)
        curr_dict()[fil] = f(d)
        return True
    return False

def fread(args=[]):
    '''fread <file>'''
    if len(args)!=1: return usage(fread)
    d=str(args[0])
    if curr_dict().has_key(d) and isFile(curr_dict()[d]):
        r=getFile(curr_dict()[d])
        if r:
            p=getpass()
            r=decrypt(r, p)
            return r
    return None
    
def oldedit(args=[]):
    '''oldedit <file> (old version of edit)'''
    if len(args)!=1: return usage(oldedit);
    d=str(args[0])
    if not curr_dict().has_key(d): curr_dict()[d] = f('')
    if curr_dict().has_key(d) and isFile(curr_dict()[d]):
        i=0; r='';
        while True:
            i+=1
            s=raw_input(str(i).ljust(4)+'| ')
            if not s: break
            r+=str(s)+'\n'
        #curr_dict()[d] = f(r)
        fwrite([d,r])
    elif curr_dict().has_key(d) and isDirectory(curr_dict()[d]):
        _print('directory')
    else:
        _print('error')
         
def cat(args=[]):
    '''cat <file>'''
    if len(args)!=1: return usage(cat);
    d=str(args[0])
    if curr_dict().has_key(d) and isFile(curr_dict()[d]):
        #r=getFile(curr_dict()[d]); i=0;
        r=fread([d]); i=0;
        if not r is None:
            for l in r.replace('\r\n','\n').split('\n'):
                i+=1
                if l: _print(str(i).ljust(4)+'| '+str(l))
    elif curr_dict().has_key(d) and isDirectory(curr_dict()[d]):
        _print('directory')
    else:
        _print('error')
        
def rm(args=[]):
    '''rm <file/directory>'''
    if len(args)!=1: return usage(rm);
    d=str(args[0])
    if curr_dict().has_key(d):
        del curr_dict()[d]
        _print('delete')

def ren(args=[]):
    '''rename <file/directory> <new name>'''
    if len(args)!=2: return usage(ren);
    d=str(args[0])
    n=str(args[1])
    if curr_dict().has_key(d):
        curr_dict()[n] = curr_dict()[d]
        del curr_dict()[d]
        _print(d+' -> '+n)
    else:
        _print('error')
        
def search(args=[]):
    '''search <file> <pattern>'''
    if len(args)<2: return usage(search);
    fil=str(args[0]); pat=str(' '.join(args[1:]))
    if curr_dict().has_key(fil) and isFile(curr_dict()[fil]):
        r=fread([fil]); i=0;
        if not r is None:
            for l in r.replace('\r\n','\n').split('\n'):
                i+=1
                if pat in str(l).replace('\r\n','').replace('\n',''):
                    _print(str(i).ljust(4)+'| '+str(l))
        else:
            _print('file read failed')
    else:
        _print('use a file')

def info(args=[]):
    '''info <file/directory>'''
    if len(args)!=1: return usage(info);
    fil=str(args[0]);
    if curr_dict().has_key(fil) and curr_dict()[fil].has_key('meta'):
        for k in curr_dict()[fil]['meta'].keys(): _print(str(' '+str(k).ljust(20)+' : '+str(curr_dict()[fil]['meta'][k])))
    else:
        _print('file/directory not exist')
    
def listcommands(args=[]):
    '''show this help'''
    if len(args): return usage(listcommands)
    global _cmds_
    for c in _cmds_.keys(): _print(str(' '+str(c).ljust(20)+' : '+str(usage(_cmds_[c],False))))

def _login(args=[]):
    '''login without interaction'''
    if len(args)!=2: return usage(_login)
    u,p=str(args[0]),str(args[1])
    if u and p:
        setv('auth','name',u)
        setv('auth','pass',p)
        setv('user','name',u)
        refpre()
        loginhash()
        return True
    return False

def login(args=[]):
    '''login'''
    try:
        u=raw_input('username> ')
    except Exception: u=''
    try:
        p=input_pass('password> ')
    except Exception: p=''
    return _login([u,p])

def loginhash(args=[]):
    '''loginhash'''
    visualize(getv('auth','name')+getv('auth','pass'))

def load(args=[]):
    '''load <filesystem/load var>'''
    def _load(ff):
        try:
            fs=pickle.load(open(str(ff),'rb'))
        except Exception,e:
            _print(e)
            return False
        if isinstance(fs, dict):
            setv('shell','fs',fs)
            setv('shell','out',str(ff))
            return True
        return False
    f=getv('shell','out')
    if not f is None and len(args)<1:
        return _load(f)
    if len(args)!=1: return usage(load)
    return _load(args[0])
def save(args=[]):
    '''save <filesystem/load var>'''
    def _save(to):
        #if os.path.exists(to):
        #    try:
        #        os.remove(to)
        #    except Exception,e: _print(e)
        try:
            pickle.dump(getv('shell','fs'), open(str(to),'wb'))
            return True
        except Exception,e: _print(e)
        return False
    f=getv('shell','out')
    if not f is None and len(args)<1:
        return _save(f)
    if len(args)!=1: return usage(save)
    return _save(args[0])

def end(args=[]):
    '''exit'''
    return 'exit'

def clear(args=[]):
    '''clear screen'''
    os.system('cls' if os.name=='nt' else 'clear')
    return

def importfile(args=[]):
    '''import <outside src-file> <inside dst-file>'''
    if len(args)!=2: return usage(importfile);
    s=str(args[0]); d=str(args[1]);
    try:
        ff=open(s,'rb')
        fi=ff.read()
        ff.close()
    except Exception,e:
        _print('error:',e)
        return False;
    return fwrite([d,str(fi)])
def exportfile(args=[]):
    '''export <inside src-file> <outside dst-file>'''
    if len(args)!=2: return usage(exportfile);
    s=str(args[0]);
    if curr_dict().has_key(s) and isFile(curr_dict()[s]):
        d=str(args[1]); r=fread([s]);
        if not r is None:
            try:
                ff=open(d,'wb')
                ff.write(str(r))
                ff.close()
            except Exception:
                _print('error while writing'); return False;
            return True;
        else:
            _print('error while reading'); return False;
    else:
        _print('not a file / not existent')

def edit(args=[]):
    '''edit <file> with a editor'''
    if len(args)!=1: return usage(edit);
    d=str(args[0])
    f='.edit_vfs.tmp'
    if curr_dict().has_key(d) and isFile(curr_dict()[d]):
        exportfile([d,f])
    elif curr_dict().has_key(d) and isDirectory(curr_dict()[d]):
        _print('directory'); return;
    editors = ('/usr/bin/gedit','/bin/nano','/usr/bin/vim','/usr/bin/vi')
    for editor in editors:
        if os.path.exists(editor):
            try:
                os.system(editor+' '+f)
                if os.path.exists(f):
                    importfile([f,d])
                    os.remove(f)
                else:
                    _print('edit nothing')
            except Exception,e: _print(e)
            break
                
def python(args=[]):
    '''execute pycode inside <file>'''
    if len(args)!=1: return usage(python);
    d=str(args[0]);
    if curr_dict().has_key(d) and isFile(curr_dict()[d]):
        o='.pymodule_vfs'
        f=o+'.tmp'
        exportfile([d,f])
        if os.path.exists(f):
            def deletepymodule():
                for suf in ('.pyc','.pyo','.py','.tmp'):
                    if os.path.exists(o+suf): os.remove(o+suf)
            try:
                os.system('python '+f)
            except Exception,e:
                _print(e)
            deletepymodule()
    elif curr_dict().has_key(d) and isDirectory(curr_dict()[d]):
        _print('directory')
    else:
        _print('error')

def sleep(args=[]):
    '''sleep <seconds>'''
    if len(args)!=1: return usage(sleep)
    try:
        t=int(args[0])
        # cannot catch keyboardinterrupt. so workaround:
        os.system('python -c "from time import sleep; sleep('+str(t)+');"')
    except Exception: pass
    
def visualize(d):
    h=hashcheck(d)
    e=h[:4]; z=h[4:8]; d=h[8:12]; v=h[12:16]; a='abcdefghijklm01234';
    e=list(e); z=list(z); d=list(d); v=list(v); s=''; plus='+ '; minus='- ';
    for m in e:
        s+=plus if m in a else minus
    _print(s); s='';
    for m in z:
        s+=plus if m in a else minus
    _print(s); s='';
    for m in d:
        s+=plus if m in a else minus
    _print(s); s='';
    for m in v:
        s+=plus if m in a else minus
    _print(s);

_cmds_ = {
    'exit':end,
    'ls':ls,
    'cd':cd,
    'mkdir':mkdir,
    'rmdir':rmdir,
    'rm':rm,
    'ren':ren,
    'login':login,
    'loginhash':loginhash,
    'load':load,
    'save':save,
    'help':listcommands,
    'edit':edit,
    'cat':cat,
    'clear':clear,
    'search':search,
    'info':info,
    'import':importfile,
    'export':exportfile,
    'python':python,
    'sleep':sleep
}

def main():
    #head
    print(' /### ### ### ### ### ### ###\\ ')
    print('|                             |')
    print('|   simple virtual            |')
    print('|            file system      |')
    print('|                        by s.|')
    print(' \\### ### ### ### ### ### ###/ ')
    login()
    refpre()
    while True:
        try:
            raw = raw_input(getpre())
        except Exception: raw = ''
        try:
            cmd = raw.split()[0]
            if cmd and cmd in _cmds_:
                try:
                    if _cmds_[cmd](raw.split()[1:])=='exit': break
                except Exception,e: _print(e)
        except Exception,e: pass
    v=raw_input('save vfs> ')
    if v: save([v])

def parse(x):
    cmds=_cmds_
    cmds['login'] = _login
    for cmd in str(x).replace('\r\n','\n').split('\n'):
        try:
            f=cmd.split()[0]
            if f and f in cmds:
                if cmds[f](cmd.split()[1:])=='exit': break
        except Exception: pass

if __name__=='__main__': main()
